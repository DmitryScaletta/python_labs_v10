﻿# 5. Написать программу на языке Python, создающую из русско-английского словаря англо-русский словарь (обязательно использовать словари(dict)). 
# Входные данные берутся из файла input.txt, выходные данные записываются в файл output.txt. 
# Входные данные лексикографически отсортированы, и выходные данные тоже должны быть отсортированы! 
# В выходной файл первым записать полученное количество английских слов!
# Необходимо, чтобы во входном файле находилось, как минимум, 5 русских слов, которые имеют несколько английских значений. 
# На хорошую оценку по работе (8, 9 и 10) слова должны быть подобраны так, как в моём примере, чтобы в результате одно английское слово имело несколько русских значений.

input_file = open('input.txt', 'r', encoding='utf-8')
words_count = input_file.readline()
dictionary = {}

for line in input_file:
	words = line.split('-')
	rus_word  = words[0].strip()
	eng_words = map(lambda word: word.strip(), words[1].split(','))
	for eng_word in eng_words:
		if eng_word not in dictionary:
			dictionary[eng_word] = []
		dictionary[eng_word].append(rus_word)

input_file.close()

for word in dictionary:
	dictionary[word].sort()

output_file = open('output.txt', 'w', encoding='utf-8')
output_file.write(str(len(dictionary)) + '\n')
for eng_word in sorted(dictionary):
	output_file.write(eng_word + ' - ' + ', '.join(dictionary[eng_word]) + '\n')
