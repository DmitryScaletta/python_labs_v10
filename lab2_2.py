# 2. На языке Python 3 разработать 2 программы (модули) для обработки одномерных массивов (векторов), используя списки. Одна программа должна работать с целочисленным вектором, а вторая с вещественным вектором.

# 10. 2) Дан целочисленный вектор А(n). Подсчитайте сколько раз встречается в этом векторе минимальное по величине число.

a = []
try:
	n = int(input('Enter a size: '))
except ValueError:
	print('Please, enter a number')
	exit()

if n < 1:
	print('Please, enter value more than 0')
	exit()

for i in range(1, n + 1):
	while True:
		try:
			t = float(input('a[{:d}] = '.format(i)))
		except ValueError:
			print('Please, enter a number')
		else:
			break
	a.append(t)

print('\nSource vector')
for i in range(0, n):
	print('%.2f'%a[i], end=" ")

min_v = a[0]
count = 0
for i in range(0, n):
	if a[i] < min_v:
		min_v = a[i]
		count = 0
	if a[i] == min_v:
		count += 1

print('\nCount of min elements:', count)
