# 6. Написать программу на языке Python, реализующую решение задачи с использованием строкового типа данных. Программу просчитать для различных исходных данных.

# Вариант 10
# Определить сколько раз в тексте встречаются слова максимальной длины.

s = input('Enter a string: ')

words = s.split(' ')

max_words = set()
max_l     = 0
for word in words:
	length = len(word)
	if length > max_l:
		max_l = length

for word in words:
	if len(word) == max_l:
		max_words.add(word)

print('Word max length:',  max_l)

for word in max_words:
	print('Word:', word, '\tcount:', words.count(word))
