# 3. Разработанные во втором задании программы оформить, с использовани-ем пользовательских функций. Обработать все возможные (по возможности) ошибки пользовательского ввода.

def enter_int(message):
	while True:
		try:
			n = int(input(message))
		except ValueError:
			print('Please, enter a number')
		else:
			return n
		

def create_b(a):
	b = []
	for i in range(0, len(a)):
		if a[i] < 0:
			b.append(a[i])
	for i in range(0, len(a)):
		if a[i] >= 0:
			b.append(a[i])
	return b


def display_list(a):
	for i in range(0, len(a)):
		print('%i'%a[i], end=" ")


a = []
n = enter_int('Enter a size: ')

if n < 1:
	print('Please, enter value more than 0')
	exit()

for i in range(1, n + 1):
	t = enter_int('a[{:d}] = '.format(i))
	a.append(t)

print('\nSource vector')
display_list(a)

b = create_b(a)

print('\nResult vector')
display_list(b)
