# 2. На языке Python 3 разработать 2 программы (модули) для обработки одномерных массивов (векторов), используя списки. Одна программа должна работать с целочисленным вектором, а вторая с вещественным вектором.

# 10. 1) Дан целочисленный вектор А(n). Построить вектор B(n), который содержит те же числа, что и вектор А(n), но в котором все отрицательные элементы предшествуют всем неотрицательным.

a = []
try:
	n = int(input('Enter a size: '))
except ValueError:
	print('Please enter a number')
	exit()

if n < 1:
	print('Please enter value more than 0')
	exit()

for i in range(1, n + 1):
	while True:
		try:
			t = int(input('a[{:d}] = '.format(i)))
		except ValueError:
			print('Please enter a number')
		else:
			break
	a.append(t)

print('\nSource vector')
for i in range(0, n):
	print('%i'%a[i], end=" ")

b = []
for i in range(0, n):
	if a[i] < 0:
		b.append(a[i])
for i in range(0, n):
	if a[i] >= 0:
		b.append(a[i])

print('\nResult vector')
for i in range(0, n):
	print('%2i'%b[i], end=" ")
