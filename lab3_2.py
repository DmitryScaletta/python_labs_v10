# 3. Разработанные во втором задании программы оформить, с использовани-ем пользовательских функций. Обработать все возможные (по возможности) ошибки пользовательского ввода.

def enter_int(message):
	while True:
		try:
			n = int(input(message))
		except ValueError:
			print('Please, enter a number')
		else:
			return n


def enter_float(message):
	while True:
		try:
			n = float(input(message))
		except ValueError:
			print('Please, enter a number')
		else:
			return n	


def min_count(a):
	min_v = a[0]
	count = 0
	for i in range(0, len(a)):
		if a[i] < min_v:
			min_v = a[i]
			count = 0
		if a[i] == min_v:
			count += 1
	return count


def display_list(a):
	for i in range(0, len(a)):
		print('%.2f'%a[i], end=" ")


a = []
n = enter_int('Enter a size: ')

if n < 1:
	print('Please, enter value more than 0')
	exit()

for i in range(1, n + 1):
	t = enter_float('a[{:d}] = '.format(i))
	a.append(t)

print('\nSource vector')
display_list(a)

print('\nCount of min elements:', min_count(a))
